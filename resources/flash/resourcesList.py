resourcesList = (
    ("Magisk", (
        {
            "show": "v25.2",
            "name": "Magisk_v25.2.zip",
            "version": "v25.2",
            "message": "Magisk是一套用于定制Android并且自带Root的开源工具",
            "path": r"Magisk_v25.2.zip"
        },
        {
            "show": "v21.1",
            "name": "Magisk_v21.1.zip",
            "version": "v21.1",
            "message": "Magisk是一套用于定制Android并且自带Root的开源工具",
            "path": r"Magisk_v21.1.zip"
        },
        {
            "show": "v21.0(phh)",
            "name": "Magisk_v21.0(phh).zip",
            "version": "v21.0(phh)",
            "message": "适用于Phh系统的Magisk",
            "path": r"Magisk_v21.0(phh).zip"
        },
        {
            "show": "v20.4",
            "name": "Magisk_v20.4.zip",
            "version": "v20.4",
            "message": "Magisk是一套用于定制Android并且自带Root的开源工具",
            "path": r"Magisk_v20.4.zip"
        },
        {
            "show": "v20.3(phh)",
            "name": "Magisk_v20.3(phh).zip",
            "version": "v20.3(phh)",
            "message": "适用于Phh系统的Magisk",
            "path": r"Magisk_v20.3(phh).zip"
        },
        {
            "show": "v19.3",
            "name": "Magisk_v19.3.zip",
            "version": "v19.3",
            "message": "适用于Phh系统的Magisk",
            "path": r"Magisk_v19.3.zip"
        },
        {
            "show": "Uninstaller",
            "name": "Magisk_uninstaller_20201003.zip",
            "version": "20201003",
            "message": "Mgaisk卸载包，刷入会卸载Magisk并清除所有已安装模块",
            "path": r"Magisk_uninstaller_20201003.zip"
        },
    )),
    ("Riru", (
        {
            "show": "Core v21.3",
            "name": "Riru_Core_v21.3.zip",
            "version": "v21.3",
            "message": "Riru核心代码",
            "path": r"Riru_Core_v21.3.zip"
        },
        {
            "show": "EdXp(SH) v0.4.6.1",
            "name": "Riru_EdXposed(SandHook)_v0.4.6.1.zip",
            "version": "v0.4.6.1",
            "message": "新一代Xposed框架",
            "path": r"Riru_EdXposed(SandHook)_v0.4.6.1.zip"
        },
    ))

)
