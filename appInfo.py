appinfo = {
    "appName": "畅玩6X工具箱",
    "about": "畅玩6X工具箱是一款以荣耀畅玩6X为测试机型做的工具箱",
    "message": """畅玩6X工具箱是一款以荣耀畅玩6X为测试机型做的工具箱""",
    "gitUrl": "https://gitee.com/Jesse205/Honor6XTools",
    "author": "杰西205",
    "version": "2.0(final)",
    "versionCode": 2099,
    "platform": "x64",
    "license": [
        ("Scrcpy", "https://github.com/Genymobile/scrcpy"),
    ],
    "toolsVer": [
        ("Scrcpy", "v1.24"),
        ("Android SDK Platform Tools", "v33.0.0 (Packed after Jesse205)"),
        ("IDT", "2.0.0.9"),
        ("PotatoNV", "v2.2.1"),
        ("Huawei Update Extractor", "0.9.9.5"),
        ("Firmware Finder", "2.12.17.3"),
        ("HUAWEI  Multi-Upgrade Software", "?")
    ],

}
