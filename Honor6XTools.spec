# -*- mode: python ; coding: utf-8 -*-
import os

# Smart Build Infomation File by Jesse205
_hiddenimports = ["about"]
_datas = [('icon.ico', '.'),('Android_SDK_Platform_Tools_v33.0.0 (Packed after Jesse205).exe', '.'), ('resources', 'resources'), ('shellTools', 'shellTools'),('tools', 'tools')]
for _root, _dirs, _files in os.walk("sub", topdown=False):
    for _name in _files:
        _spname = os.path.splitext(_name)
        if "__pycache__//" not in _spname:
            if _spname[1] == ".py":
                _hiddenimports.append(os.path.join(_root, _spname[0]).replace("\\", "."))
            else:
                _datas.append((os.path.join(_root, _name),_root),)

block_cipher = None

SETUP_DIR = r'F:\my\编程\Python\工程\畅玩6X工具箱\\'

a = Analysis(['Honor6XTools.py'],
             pathex=[SETUP_DIR],
             binaries=[],
             datas=_datas,
             hiddenimports=_hiddenimports,
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

pyz = PYZ(a.pure, a.zipped_data,
          cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='Honor6XTools',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=False,
          console=False, icon='icon.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='Honor6XTools')
