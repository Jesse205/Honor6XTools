import clickEvents

tools = (
    (
        "刷机",
        (
            (".APP提取", clickEvents.HuaweiUpdateExtractor, "Huawei Update Extractor，解压打包华为固件 (UPDATE.APP) 的工具\n功能：从 APP 文件 提取 IMG 文件"),
            ("IDT救砖", clickEvents.IDT, "Image Download Tool，能不用最好不用\n功能：救砖"),
            ("Sideload", clickEvents.sideload, "一种很方便刷入位于电脑的刷机包的工具\n功能：刷入 ZIP 刷机包"),
            ("Fastboot刷机", None, "一种很方便刷入位于电脑的IMG镜像的工具\n功能：刷入 IMG"),
            ("FF固件下载器", clickEvents.FirmwareFinder, "Firmware Finder 固件下载，可能会出现闪退的问题，重新加入即可\n功能：下载 ROM"),
            ("华为降级工具", clickEvents.MultiUpgradeSoftwarer, "密码：Huawei\n从火焰头皮屑那里拿的"),
            ("PotatoNV", clickEvents.PotatoNV, "一款解锁工具\n功能：解 Bootloader 锁"),
            ("解锁回锁", None, "解/回 Bootloader 锁"),
        )
    ),
    (
        "控制",
        (
            ("高级重启", clickEvents.reboot, "快速重启手机到各个模式，不想长按按键可以使用试试这个工具\n功能：重启手机"),
            ("命令提示符", clickEvents.cmd, "做一些黑客操作的窗口\n功能：无"),
            ("Shell终端", clickEvents.shell, "做一些黑客操作的窗口，只不过用的是你的手机\n功能：调试手机"),
            ("Scrcpy投屏", clickEvents.scrcpy, "荣耀畅玩6X不支持多屏协同，这个工具是不错的代替\n功能：手机投屏、屏幕录制"),
            ("安装APK", None, "一键安装APK需要关闭“监控 ADB 安装用应用”\n主要功能：安装 APK, APKS"),
            ("应用管理", None, "可以在这里禁用系统应用\n功能：冻结软件，禁用系统功能，恢复系统软件"),
            ("有线上网", None, "使用有线上网，网速更快\n功能：通过数据线将电脑网络共享给手机"),
            ("WIFI管理", None, "功能：管理 WIFI，查看 WIFI 密码"),
        )
    ),
    (
        "配置",
        (
            ("用户界面调节", clickEvents.systemUI, "调节手机屏幕的，日常使用没必要调节\n功能：隐藏导航栏，调节分辨率"),
            ("强制网络门户", None, "Captive Protal，去除叹号要用这个\n功能：去除WIFI图标叹号，更改Captive Protal 服务器"),
            ("启动Shizuku", clickEvents.startShizuku, "以 ADB 方式一键激活，相对于Root激活又一定的限制\n功能：使 Shizuku 获得ADB权限"),
            ("动画速率调节", None, "比系统自带的范围广，精度高\n功能：关闭动画，微调动画速率"),
        )
    )
)
