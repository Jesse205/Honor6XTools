from tkinter.ttk import Label


class MyIcon(Label):
    def __init__(self, master=None, icon=None, **kw):
        super().__init__(master, text=icon, font="assets\\SegMDL2.ttf", anchor="center", **kw)
