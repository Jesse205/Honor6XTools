from tkinter import Menu, END
from tkinter.ttk import Entry, Combobox, Spinbox


class baseEvent():
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        menu = Menu(tearoff=False)
        menu.add_command(label='剪切', command=self.cut)
        menu.add_command(label='复制', command=self.copy)
        menu.add_command(label='粘贴', command=self.paste)
        menu.add_separator()
        menu.add_command(label='全选', command=self.selectAll)
        self.menu = menu
        self.bind("<ButtonRelease-3>", self.showMenu)

    def cut(self):
        self.event_generate("<<Cut>>")

    def copy(self):
        self.event_generate("<<Copy>>")

    def paste(self):
        self.event_generate('<<Paste>>')

    def selectAll(self):
        self.selection_range(0, END)

    def showMenu(self, event):
        self.menu.post(event.x_root, event.y_root)


class MyEntry(baseEvent, Entry):
    def __init__(self, master=None, widget=None, *args, **kw):
        super().__init__(master, widget, *args, **kw)


class MyCombobox(baseEvent, Combobox):
    def __init__(self, master=None, *args, **kw):
        super().__init__(master, *args, **kw)


class MySpinbox(baseEvent, Spinbox):
    def __init__(self, master=None, *args, **kw):
        super().__init__(master, *args, **kw)
