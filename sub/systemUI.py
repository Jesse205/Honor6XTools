from Jesse205 import *

window = MyWindow()

window.title("用户界面调节")
window.resizable(False, False)
window.fixDpi()
mainFrame = Frame(window)
mainFrame.pack(padx=5, pady=5)


def setOverscan(*args):
    AppEvent.overscanSet(window, left=overscanLeftEntry.get(), top=overscanTopEntry.get(),
                         right=overscanRightEntry.get(),
                         bottom=overscanBottomEntry.get())


overscanLabel = LabelFrame(mainFrame, text="Overscan 调节")
overscanLabel.pack(padx=5, pady=5, side=LEFT)
overscanFrame = Frame(overscanLabel)
overscanFrame.pack(padx=10, pady=10)

modelWidth = 9 * 25
modelHeight = 16 * 25
overscanPhoneModel = Canvas(overscanFrame, height=modelHeight, width=modelWidth, )  # background="white"
overscanPhoneModel.create_rectangle(2, 2, modelWidth + 1, modelHeight + 1, width=1)
overscanPhoneModel.grid(row=0, column=0, padx=30, pady=10)

overscanTopEntry = MySpinbox(overscanFrame)
overscanBottomEntry = MySpinbox(overscanFrame)
overscanLeftEntry = MySpinbox(overscanFrame)
overscanRightEntry = MySpinbox(overscanFrame)

overscanTopEntry.grid(row=0, column=0, sticky=N)
overscanBottomEntry.grid(row=0, column=0, sticky=S)
overscanLeftEntry.grid(row=0, column=0, sticky=W)
overscanRightEntry.grid(row=0, column=0, sticky=E)

overscanButtonsFrame = Frame(overscanFrame)
overscanButtonsFrame.grid(row=0, column=0)
Button(overscanButtonsFrame, text="调节", width=10,
       command=setOverscan, default=ACTIVE).pack(ipady=5, pady=5)
Button(overscanButtonsFrame, text="还原", width=10, command=lambda: AppEvent.overscanSet(window)).pack(ipady=5, pady=5)

for overscanEntry in [overscanTopEntry, overscanBottomEntry, overscanLeftEntry, overscanRightEntry]:
    overscanEntry["width"] = 5
    overscanEntry["from_"] = -9999
    overscanEntry["to"] = 9999
    overscanEntry.insert(END, 0)
    overscanEntry.bind("<Return>", setOverscan)

moreFrame = Frame(mainFrame)
moreFrame.pack(padx=5, pady=5, side=LEFT, fill=Y)


def setSize():
    AppEvent.setResolvingPower(window, "%sx%s" % (sizeWidthEntry.get(), sizeHeightEntry.get()))


def reSize():
    AppEvent.setResolvingPower(window, "reset")


sizeLabel = LabelFrame(moreFrame, text="分辨率调节")
sizeLabel.pack(padx=5, pady=5, fill=X, anchor=N)
sizeFrame = Frame(sizeLabel)
sizeFrame.pack(padx=5, pady=5)

sizeWidthEntry = MySpinbox(sizeFrame, width=5, from_=0, to=9999)
sizeWidthEntry.grid(row=0, column=0, padx=5, pady=5)
sizeWidthEntry.insert(END, "1080")
Label(sizeFrame, text="x").grid(row=0, column=1, pady=5)
sizeHeightEntry = MySpinbox(sizeFrame, width=5, from_=0, to=9999)
sizeHeightEntry.grid(row=0, column=2, padx=5, pady=5)
sizeHeightEntry.insert(END, "1920")

buttonFrame = Frame(sizeFrame)
buttonFrame.grid(row=1, column=0, pady=5, columnspan=3)
Button(buttonFrame, text="调节", command=setSize, width=7).pack(padx=5, side=LEFT)
Button(buttonFrame, text="还原", command=reSize, width=7).pack(padx=5, side=LEFT)


def setDpi():
    AppEvent.setDpi(window, dpiEntry.get())


def reDpi():
    AppEvent.setDpi(window, "reset")


dpiLabel = LabelFrame(moreFrame, text="DPI调节")
dpiLabel.pack(padx=5, pady=5, fill=X, anchor=N)
dpiFrame = Frame(dpiLabel)
dpiFrame.pack(padx=5, pady=5)
dpiEntry = MySpinbox(dpiFrame, width=15, from_=0, to=999)
dpiEntry.grid(padx=5, pady=5, row=0, column=0, columnspan=2, sticky="news")
dpiEntry.insert(END, "480")
Button(dpiFrame, text="修改", command=setDpi, width=7).grid(padx=5, pady=5, row=1, column=0)
Button(dpiFrame, text="还原", command=reDpi, width=7).grid(padx=5, pady=5, row=1, column=1)

window.centerWindow()
window.iconbitmap("icon.ico")

window.mainloop()
