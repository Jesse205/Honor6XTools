from Jesse205 import *
from threading import Thread
#import re
#window = MyWindow()
#window.title("设备管理")
#window.fixDpi()
#window.resizable(False, False)

# mainFrame = Frame()
# mainFrame.pack(padx=5, pady=5, side=RIGHT)

def loadPage(mainFrame,window):
    mainFrame = Frame(mainFrame)
    mainFrame.pack(side=LEFT,anchor=N)
    def refreshDevices(mode, combobox):
        devices = AndroidUtil.ADB.getDevicesList(mode)
        """
        devicesList = []
        devicesList.clear()
        devicesList.append("(auto)")
        
        for index, content in devices.items():
            devicesList.append("%s(%s)" % (index, content))"""
        devicesList = list(devices.keys())
        devicesList.insert(0, "(auto)")
        #combobox["state"] = NORMAL
        combobox["value"] = tuple(devicesList)
        del devicesList


    def refreshDeviceLabel(mode, label):
        device = AndroidUtil.ADB.getDeviceByMode(mode)
        label["text"] = "当前%s设备：%s" % (mode.capitalize(), device)
        return device


    def switchDevice(mode, combobox, label):
        device = combobox.get()
        AndroidUtil.ADB.device[mode] = device
        refreshDeviceLabel(mode, label)


    def addDevicePicker(setType):
        def switchSelfDevice(*args):
            switchDevice(setType, deviceCombobox, deviceLabel)

        deviceLabel = LabelFrame(mainFrame)
        deviceLabel.pack(fill=X, expand=YES, pady=5, padx=5)
        device = refreshDeviceLabel(setType, deviceLabel)
        deviceFrame = Frame(deviceLabel)
        deviceFrame.pack(fill=X, expand=YES, pady=5, padx=5)

        deviceCombobox = MyCombobox(deviceFrame, width=30, value=("(auto)","加载中"))
        deviceCombobox.grid(row=0, column=0, padx=5)
        deviceCombobox.insert(END, device)
        #deviceCombobox["state"] = DISABLED
        # deviceCombobox.bind("<<ComboboxSelected>>", lambda event: onChooseDevice(event, deviceCombobox))
        deviceCombobox.bind("<Return>", switchSelfDevice)
        #refreshDevices(setType, deviceCombobox)
        thread=Thread(target=refreshDevices,args=[setType,deviceCombobox])
        thread.start()
        Button(deviceFrame, text="刷新", width=4, command=lambda: refreshDevices(setType, deviceCombobox)).grid(row=0,
                                                                                                              column=1,
                                                                                                              padx=5,
                                                                                                              pady=5)
        Button(deviceFrame, text="切换", command=switchSelfDevice,
               default=ACTIVE).grid(row=1, column=0, padx=5, pady=5, ipady=5, columnspan=2, sticky="news")
        return deviceCombobox


    adbCombobox = addDevicePicker("adb")
    addDevicePicker("fastboot")

    # moreText=Separator(mainFrame)
    # moreText.pack(padx=5, pady=5, fill=BOTH)

    connectWebDeviceLabel = LabelFrame(mainFrame, text="连接网络设备")
    connectWebDeviceLabel.pack(fill=X, expand=YES, pady=5, padx=5)
    connectWebDevicFrame = Frame(connectWebDeviceLabel)
    connectWebDevicFrame.pack(fill=X, expand=YES, pady=5, padx=5)
    connectWebDevicEntryFrame = Frame(connectWebDevicFrame)
    connectWebDevicEntryFrame.pack(pady=5, fill=X, expand=YES)
    Label(connectWebDevicEntryFrame, text="IP:").pack(side=LEFT, pady=5, padx=5)
    ipEntry = MyEntry(connectWebDevicEntryFrame, width=15)
    ipEntry.pack(side=LEFT, fill=X, expand=YES, pady=5, padx=5)
    Label(connectWebDevicEntryFrame, text="端口:").pack(side=LEFT, pady=5, padx=5)
    portEntry = MySpinbox(connectWebDevicEntryFrame, width=6, from_=0, to=65535)
    portEntry.pack(side=LEFT, pady=5, padx=5)
    portEntry.insert(END, "5555")


    def connectWebDevice(*args):
        AppEvent.contentWebDevice(window, ipEntry.get(), portEntry.get())
        refreshDevices("adb", adbCombobox)


    Button(connectWebDevicFrame, text="连接", command=connectWebDevice).pack(fill=X, expand=YES, padx=5, ipady=5, side=LEFT,
                                                                           pady=5)
    ipEntry.bind("<Return>", connectWebDevice)
    portEntry.bind("<Return>", connectWebDevice)
    #deviceCombobox.current(0)

if __name__ == '__main__':
    window = MyWindow()
    window.title("设备管理")
    window.fixDpi()
    window.resizable(False, False)
    mainFrame = Frame(window)
    mainFrame.pack(padx=5, pady=5, side=RIGHT)
    loadPage(mainFrame, window)
    window.centerWindow()
    window.iconbitmap("icon.ico")
    window.mainloop()






