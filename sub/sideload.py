from Jesse205 import *
import resources.flash.resourcesList as resourcesList

window = MyWindow()
window.title("Sideload刷机")
window.fixDpi()
window.resizable(False, False)

mainFrame = Frame(window)
mainFrame.pack(padx=5, pady=5, fill=BOTH, expand=YES)


def pickFile(path=None):
    if not path:
        oldPath = entryVar.get()
        path = filedialog.askopenfilename(master=window, filetypes=[("ZIP压缩包", ".zip"), ('所有文件', '.*')],
                                          initialdir=os.path.dirname(oldPath),
                                          initialfile=os.path.basename(oldPath)).replace("/", "\\")
    if path:
        entryVar.set(path)
    entry.focus()


def startFlash(*args):
    path=entryVar.get()
    if not path:
        messagebox.showinfo("提示", "请输入文件路径", master=window)
        return
    if AppEvent.checkRunAdbCommand(window):
        MyUtils.startExe("shellTools", "sideload.bat", values="%s %s" % (path, AndroidUtil.ADB.device2cmd("adb")))

topLabelFrame = LabelFrame(mainFrame, text="文件路径")
topLabelFrame.pack(fill=X, expand=YES, pady=5, padx=5)
topFrame = Frame(topLabelFrame)
topFrame.pack(fill=X, expand=YES, pady=10, padx=5)

entryVar = StringVar(window)
entry = MyEntry(topFrame, textvariable=entryVar, width=40)
entry.pack(fill=X, expand=YES, side=LEFT, padx=5)
entry.bind("<Return>", startFlash)
Button(topFrame, text="选择", command=pickFile, width=4).pack(expand=YES, anchor=CENTER, padx=5)

Button(mainFrame, text="开始刷机", command=startFlash, default=ACTIVE).pack(padx=15, fill=BOTH, ipady=5)

warningFrame = Frame(mainFrame)
warningFrame.pack(anchor=W, fill=X, padx=15, pady=5)
MyIcon(warningFrame, icon="\uE7BA").pack(side=LEFT)
Label(warningFrame, text="刷机前请先在手机上打开Sideload模式", anchor="center").pack(side=LEFT)

moreFrame = MyToolsFrame(mainFrame)
moreFrame.setMaxButtonColumn(4)
moreFrame.pack(fill=X, expand=YES)


def buildPickEvent(path):
    def pick():
        pickFile("..\\resources\\flash\\"+path)

    return pick


toolTipTemplate = """名称：{name}
版本：{version}

{message}"""
for group in resourcesList.resourcesList:
    moreFrame.addGroup(title=group[0])
    for item in group[1]:
        button = moreFrame.addButton(text=item["show"],
                                     command=buildPickEvent(item["path"]))
        ToolTip(button,
                text=toolTipTemplate.format(name=item["name"], version=item["version"], message=item["message"]))

"""
moreFrame.addGroup(title="Magisk")

button=moreFrame.addButton(text="Magisk v20.4", width=14, command=lambda: pickFile("assets\\flash\\Magisk-v20.4(20400).zip"))
ToolTip(button,text="Magisk是一套用于定制Android并且自带Root的开源工具")

button=moreFrame.addButton(text="Magisk v21.0", width=15, command=lambda: pickFile("assets\\flash\\Magisk-v21.0.zip"))
ToolTip(button,text="Magisk是一套用于定制Android并且自带Root的开源工具")

button=moreFrame.addButton(text="Magisk v20.3 phh", width=14, command=lambda: pickFile("assets\\flash\\Magisk-v20.3 phh.zip"))
ToolTip(button,text="适用于Phh系统的Magisk(新版本Phh系统不能用)")

button=moreFrame.addButton(text="Magisk v21.0 phh", width=14, command=lambda: pickFile("assets\\flash\\Magisk-v21.0-phh.zip"))
ToolTip(button,text="适用于Phh系统的Magisk")

button=moreFrame.addButton(text="Magisk uninstaller", width=15, command=lambda: pickFile("assets\\flash\\Magisk-uninstaller-20201003.zip"))
ToolTip(button,text="卸载Magisk")

moreFrame.addGroup(title="Xposed")

button=moreFrame.addButton(text="Riru-Core v21.3", width=13, command=lambda: pickFile("assets\\flash\\Riru_Core_v21.3.zip"))
ToolTip(button,text="Riru核心代码")

button=moreFrame.addButton(text="EdXposed(SandHook) v0.4.6.1", width=25, command=lambda: pickFile("assets\\flash\\Riru_EdXposed(SandHook)_v0.4.6.1.zip"))
ToolTip(button,text="适用于Phh系统的Magisk")
"""

window.centerWindow()
window.iconbitmap("icon.ico")
window.mainloop()
