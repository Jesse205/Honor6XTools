from Jesse205 import *
import os, time
import MyUtils


window = MyWindow()
window.title("ScrcpyGUI启动器")

window.fixDpi()
window.resizable(False, False)


# 启动Scrcpy函数
def startScrcpy(*args):
    if AppEvent.checkRunAdbCommand(window):
        values = buildValues()
        print("Scrcpy", values)
        result = MyUtils.startExe(r"shellTools", r"scrcpy.exe", values=values)
        print("Scrcpy", result)
        # if result[0]:
        #     messagebox.showerror("启动失败",result[1], master=window)


# 构建Combox函数
"""
def buildTitleCombobox(master, row, title, value):
    label=Label(master, text=title, width=10)
    label.grid(padx=5, pady=5, row=row, column=0)
    entry = MyCombobox(master, value=value, width=40)
    entry.current(0)
    entry.grid(padx=5, pady=5, row=row, column=1, sticky="news")
    entry.bind("<Return>", startScrcpy)
    return label,entry


def buildTitleEntry(master, row, title, value):
    label=Label(master, text=title, width=10)
    label.grid(padx=5, pady=5, row=row, column=0)
    entry = MyEntry(master, width=42)
    entry.grid(padx=5, pady=5, row=row, column=1, sticky="news")
    entry.bind("<Return>", startScrcpy)
    return label,entry"""


def buildTitleCombobox(master, row, title, value):
    frame = Frame(master)
    frame.pack(pady=5, fill=X)
    label = Label(frame, text=title, width=10)
    label.pack(padx=5, side=LEFT)
    entry = MyCombobox(frame, value=value, width=40)
    entry.current(0)
    entry.pack(padx=5, side=LEFT, fill=X, expand=YES)
    entry.bind("<Return>", startScrcpy)
    return frame, label, entry


def buildTitleEntry(master, row, title, value):
    frame = Frame(master)
    frame.pack(pady=5, fill=X)
    label = Label(frame, text=title, width=10)
    label.pack(padx=5, side=LEFT)
    entry = MyEntry(frame, width=40)
    entry.pack(padx=5, side=LEFT, fill=X, expand=YES)
    entry.bind("<Return>", startScrcpy)
    return frame, label, entry


# 构建参数时图方便
defaultKeyEntry = {}
noDefaultKeyEntry = {}
radioVars = {}
checkVars = {}

# 根布局，为了等宽
mainFrame = Frame(window)
mainFrame.pack(padx=5, pady=5, fill=BOTH, expand=YES)

# 画面设置 组
captureLabel = LabelFrame(mainFrame, text="画面设置")
captureLabel.pack(padx=5, pady=5, fill=X, expand=YES)
captureFrame = Frame(captureLabel)
captureFrame.pack(padx=5, pady=5, fill=X, expand=YES)

frame, _, defaultKeyEntry["--max-size"] = buildTitleCombobox(captureFrame, 0, "缩小分辨率", ("默认", "1920", "1080", "1024"))
ToolTip(frame, text="有时候，将屏幕镜像分辨率降低可以有效提升性能\n将高度和宽度都限制在一定大小内（如 1024）")

frame, _, defaultKeyEntry["--bit-rate"] = buildTitleCombobox(captureFrame, 1, "比特率", ("默认", "8M", "6M", "2M"))
ToolTip(frame, text="默认的比特率是8Mbps\n改变画面的比特率 (比如2Mbps)")

frame, _, defaultKeyEntry["--max-fps"] = buildTitleCombobox(captureFrame, 2, "最大帧率", ("默认", "60", "45", "30", "15"))
ToolTip(frame, text="仅在Android 10和以后的版本被官方支持，但也可能在更早的版本可用")

# 屏幕朝向
frame = Frame(captureFrame)
frame.pack(pady=5, fill=X)
label = Label(frame, text="视频方向", width=10)
label.pack(padx=5, side=LEFT)
ToolTip(frame, text="该设定影响录制")

orientationRadiosFrame = Frame(frame)
orientationRadiosFrame.pack(padx=5, side=LEFT, fill=X, expand=YES)
orientations = (
    ("默认", -1),
    ("当前方向", -2),
    ("正常方向", 0),
    ("逆时针90°", 1),
    ("顺时针90°", 3),
    ("180°", 2))

orientationVar = IntVar(window)
orientationVar.set(-1)
radioVars["--lock-video-orientation"] = orientationVar
for key, value in orientations:
    Radiobutton(orientationRadiosFrame, text=key, variable=orientationVar, value=value).pack(side=LEFT, padx=0)
# END 组


# 屏幕录制 组
recordingLabel = LabelFrame(mainFrame, text="屏幕录制")
recordingLabel.pack(padx=5, pady=5, fill=X, expand=YES)
ToolTip(recordingLabel, text="在显示中“被跳过的帧”会被录制，虽然它们由于性能原因没有实时显示\n在传输中每一帧都有时间戳，所以包时延变化并不影响录制的文件")

recordingFrame = Frame(recordingLabel)
recordingFrame.pack(padx=5, pady=5, fill=X, expand=YES)

recordingCheckVar = IntVar(window, 0)
recordingCheck = Checkbutton(recordingFrame, text="开启录制", variable=recordingCheckVar, onvalue=1, offvalue=0)
recordingCheck.pack(padx=5, pady=5, anchor=W)

Label(recordingFrame, text="保存路径", width=10).pack(padx=5, pady=5, side=LEFT)
recordingEntryFrame = Frame(recordingFrame)
recordingEntryFrame.pack(pady=5, side=LEFT, fill=X, expand=YES)

recordingVar = StringVar(window)
recordingVar.set(os.path.expanduser("~\\Scrcpy_%s.mp4" % (time.strftime("%Y%m%d_%H%M%S", time.localtime()))))
recordingEntry = MyEntry(recordingEntryFrame, textvariable=recordingVar)
recordingEntry.pack(padx=5, fill=X, expand=YES, side=LEFT)
recordingEntry.bind("<Return>", startScrcpy)


def pickFile(path=None):
    if not path:
        oldPath = recordingVar.get()
        path = filedialog.asksaveasfilename(master=window, filetypes=[("MP4视频文件", ".mp4"), ('所有文件', '.*')],
                                            initialdir=os.path.dirname(oldPath),
                                            initialfile=os.path.basename(oldPath)).replace("/", "\\")
    if path:
        recordingVar.set(path)
    recordingEntry.focus()


Button(recordingEntryFrame, text="选择", command=pickFile, width=4).pack(expand=YES, anchor=E, padx=5)
# END 组

# 窗口设置 组
windowLabel = LabelFrame(mainFrame, text="窗口设置")
windowLabel.pack(padx=5, pady=5, fill=X, expand=YES)
windowFrame = Frame(windowLabel)
windowFrame.pack(padx=5, pady=5, fill=X, expand=YES)

frame, _, noDefaultKeyEntry["--window-title"] = buildTitleEntry(windowFrame, 0, "标题", None)
ToolTip(frame, text="窗口的标题默认为设备型号")

frame = Frame(windowFrame)
frame.pack(pady=5, fill=X)
label = Label(frame, text="旋转", width=10)
label.pack(padx=5, side=LEFT)
ToolTip(frame, text="这只影响显示，不影响录制")

rotateRadiosFrame = Frame(frame)
rotateRadiosFrame.pack(padx=5, side=LEFT, fill=X, expand=YES)
rotates = (
    ("默认", -1),
    ("无旋转", 0),
    ("逆时针90°", 1),
    ("顺时针90°", 3),
    ("180°", 2))

rotateVar = IntVar(window)
rotateVar.set(-1)
radioVars["--rotation"] = rotateVar
for key, value in rotates:
    Radiobutton(rotateRadiosFrame, text=key, variable=rotateVar, value=value).pack(side=LEFT, padx=0)

frame = Frame(windowFrame)
frame.pack(pady=5, fill=X)
onTopCheckVar = IntVar(window, 0)
onTopCheck = Checkbutton(frame, text="保持窗口在最前", variable=onTopCheckVar, onvalue=1, offvalue=0)
onTopCheck.pack(padx=5, side=LEFT)
checkVars["--always-on-top"] = onTopCheckVar

# 全屏
fullScreeCheckVar = IntVar(window, 0)
fullScreenCheck = Checkbutton(frame, text="全屏", variable=fullScreeCheckVar, onvalue=1, offvalue=0)
fullScreenCheck.pack(padx=5, side=LEFT)
checkVars["--fullscreen"] = fullScreeCheckVar
ToolTip(fullScreenCheck, text="全屏状态可以通过“MOD+f”实时改变。")
# END 组

# 其他设置 组
moreLabel = LabelFrame(mainFrame, text="其他设置")
moreLabel.pack(padx=5, pady=5, fill=X, expand=YES)
moreFrame = Frame(moreLabel)
moreFrame.pack(padx=5, pady=5, fill=X, expand=YES)

# 行：文字注入偏好 避免按键重复 等
group = Frame(moreFrame)
group.pack(pady=5, fill=X, expand=YES)

# 关闭控制
noControlCheckVar = IntVar(group, 0)
noControlCheck = Checkbutton(group, text="只读", variable=noControlCheckVar, onvalue=1, offvalue=0)
noControlCheck.pack(padx=5, side=LEFT)
checkVars["--no-control"] = noControlCheckVar
ToolTip(noControlCheck, text="关闭电脑对设备的控制（如键盘输入、鼠标移动和文件传输）")

# 保持常亮
stayAwakeCheckVar = IntVar(group, 0)
stayAwakeCheck = Checkbutton(group, text="保持常亮", variable=stayAwakeCheckVar, onvalue=1, offvalue=0)
stayAwakeCheck.pack(padx=5, side=LEFT)
checkVars["--stay-awake"] = stayAwakeCheckVar
ToolTip(stayAwakeCheck, text="防止设备在已连接的状态下休眠")

# 关闭设备屏幕
turnScreenOffCheckVar = IntVar(group, 0)
turnScreenOffCheck = Checkbutton(group, text="关闭设备屏幕", variable=turnScreenOffCheckVar, onvalue=1, offvalue=0)
turnScreenOffCheck.pack(padx=5, side=LEFT)
checkVars["--turn-screen-off"] = turnScreenOffCheckVar
ToolTip(turnScreenOffCheck, text="""或者在需要的时候按“MOD+o”。
要重新打开屏幕的话，需要按“MOD+Shift+o”.
在Android上,电源按钮始终能把屏幕打开。
为了方便，如果按下电源按钮的事件是通过Scrcpy发出的（通过点按鼠标右键或“MOD+p”），它会在短暂的延迟后将屏幕关闭。
物理的电源按钮仍然能打开设备屏幕。""")

# 文字注入偏好
inputGoodCheckVar = IntVar(window, 0)
inputGoodCheck = Checkbutton(group, text="文字注入偏好", variable=inputGoodCheckVar, onvalue=1, offvalue=0)
inputGoodCheck.pack(padx=5, side=LEFT)
checkVars["--prefer-text"] = inputGoodCheckVar
ToolTip(inputGoodCheck, text="""打字的时候，系统会产生两种事件：
    按键事件 ，代表一个按键被按下/松开。
    文本事件 ，代表一个文本被输入。
程序默认使用按键事件来输入字母。只有这样，键盘才会在游戏中正常运作（尤其“WASD”键）。
但这也有可能造成问题。如果您遇到了这样的问题，您可以通过勾选此项
（这会导致键盘在游戏中工作不正常）""")

# 避免按键重复
noMoreClickCheckVar = IntVar(group, 0)
noMoreClickCheck = Checkbutton(group, text="避免按键重复", variable=noMoreClickCheckVar, onvalue=1, offvalue=0)
noMoreClickCheck.pack(padx=5, side=LEFT)
checkVars["--no-key-repeat"] = noMoreClickCheckVar
ToolTip(noMoreClickCheck, text="当你一直按着一个按键不放时，程序默认产生多个按键事件。 在某些游戏中这可能会导致性能问题。")

# 行：不点亮屏幕 等
group = Frame(moreFrame)
group.pack(pady=5, fill=X, expand=YES)

# 不点亮屏幕
noMoreClickCheckVar = IntVar(group, 0)
noMoreClickCheck = Checkbutton(group, text="不点亮屏幕", variable=noMoreClickCheckVar, onvalue=1, offvalue=0)
noMoreClickCheck.pack(padx=5, side=LEFT)
checkVars["--no-power-on"] = noMoreClickCheckVar
ToolTip(noMoreClickCheck, text="默认情况下，启动时，设备将点亮屏幕")


# END 组

# 构建参数函数
def buildValues():
    result = ""
    for key, value in defaultKeyEntry.items():
        value = value.get()
        if value != "默认" and value:
            result += "%s \"%s\" " % (key, value)
    for key, value in noDefaultKeyEntry.items():
        value = value.get()
        if value:
            result += "%s \"%s\" " % (key, value)
    for key, value in radioVars.items():
        value = value.get()
        if value != -1:
            if value == -2:
                result += "%s " % (key)
            else:
                result += "%s %s " % (key, value)
    for key, value in checkVars.items():
        value = value.get()
        if value:
            result += "%s " % (key)

    result += AndroidUtil.ADB.device2cmd("adb")

    if recordingCheckVar.get():
        result += "--record \"%s\" " % recordingVar.get()
    return result


# 底部操作栏（启动按钮）
buttonsFrame = Frame(mainFrame)
buttonsFrame.pack(pady=5, fill=X)
startButton = Button(buttonsFrame, text="启动Scrcpy", width=10, default=ACTIVE, command=startScrcpy)
startButton.pack(padx=5, ipady=5, side=RIGHT)
scaleFactor = window.getScaleFactor()
if scaleFactor and scaleFactor != 100:  # 如果缩放并非100%
    warningFrame = Frame(mainFrame)
    warningFrame.pack(anchor=E)
    MyIcon(warningFrame, icon="\uE7BA").pack(side=LEFT, anchor="center")
    Label(warningFrame, text="您的电脑缩放并非100%，画面模糊属于正常现象。", justify=LEFT).pack(side=LEFT, anchor="center")
    ToolTip(warningFrame, text="您可以通过修改“adb\scrcpy-noconsole.exe”的高DPI缩放使其变得清晰")

"""sg=Sizegrip(window)
sg.pack(anchor='sw',side=RIGHT)"""





menuBar = Menu(window)
menuBar.add_command(label="ScrcpyGitHub", command=lambda: openUrl("https://github.com/Genymobile/scrcpy"))
menuBar.add_command(label="Scrcpy帮助",
                    command=lambda: openUrl("https://github.com/Genymobile/scrcpy/blob/master/README.zh-Hans.md"))

window.config(menu=menuBar)

window.centerWindow()
window.iconbitmap("icon.ico")
window.mainloop()
