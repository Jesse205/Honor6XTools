from Jesse205 import *
import MyUtils

window = MyWindow()

window.title("Shizuku启动器")
window.resizable(False, False)
window.fixDpi()
mainFrame = Frame(window)
mainFrame.pack(padx=5, pady=5)

packageName = "moe.shizuku.privileged.api"


def startShizuku(path):
    if AppEvent.checkRunAdbCommand(window):
        MyUtils.startExe("shellTools", "startShizuku.bat", values="%s %s" % (path, AndroidUtil.ADB.device2cmd("adb")))


def startShizuku2():
    startShizuku(pathVar.get())


def buildSetPathFunction(path):
    return lambda: pathVar.set(path % packageName)


paths = (
    ("Shizuku v11.2.0+", "/sdcard/Android/data/%s/start.sh"),
    ("Shizuku v4.0.0+", "/data/user_de/0/%s/start.sh"),
    ("Shizuku v4.0.0+(安卓 M)", "/data/user/0/%s/start.sh"),
    ("Shizuku v3.x", "/sdcard/Android/data/%s/files/start.sh"),
)

pathLabel = LabelFrame(mainFrame, text="start.sh路径")
pathLabel.pack(padx=5, pady=5, fill=X, expand=YES)

pathFrame = Frame(pathLabel)
pathFrame.pack(padx=5, pady=5, fill=X)

pathVar = StringVar(window)
pathVar.set(paths[0][1] % packageName)
pathEntry = MyEntry(pathFrame, width=40, textvariable=pathVar)
pathEntry.pack(padx=5, pady=5, fill=X, expand=YES)
pathEntry.bind("<Return>", startShizuku2)

toolsFrame = MyToolsFrame(pathFrame)
toolsFrame.setMaxButtonColumn(2)
toolsFrame.pack(fill=X, expand=YES)
toolsFrame.addGroup(title="推荐路径")

for content in paths:
    button = toolsFrame.addButton(text=content[0],
                                  command=buildSetPathFunction(content[1]), width=20)
    ToolTip(button, text=content[1] % packageName)

Button(mainFrame, text="启动Shizuku", command=startShizuku2, width=10, default=ACTIVE).pack(padx=5,pady=5, ipady=5, side=RIGHT)

# startShizuku()
window.centerWindow()
window.iconbitmap("icon.ico")

window.mainloop()
# window.destroy()
