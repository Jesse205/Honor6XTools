from Jesse205 import *
import tkinter as tk
from tkinter.scrolledtext import ScrolledText

window = MyWindow()
window.title("关于 %s" % appinfo["appName"])
window.fixDpi()
window.resizable(False, False)
# window.size(600,500)

version = "v%s(%s)%s" % (appinfo.get("version", "未知"), appinfo.get("versionCode", ""),appinfo.get("platform", ""))

topFrame = tk.Frame(window, background="white")
topFrame.pack(fill=X,ipadx=5, ipady=5)

iconPhoto = PhotoImage(master=window, file="resources\\icons\\icon.png")
iconLabel = tk.Label(topFrame, image=iconPhoto, width=100, height=100, background="white")
iconLabel.pack(side=LEFT, padx=5, pady=5)

appInfoFrame = tk.Frame(topFrame, background="white")
appInfoFrame.pack(side=LEFT, padx=5, pady=5)
appInfoTopFrame = tk.Frame(appInfoFrame, background="white")
appInfoTopFrame.pack(fill=X)

appNameLabel = tk.Label(appInfoTopFrame, text=appinfo["appName"], background="white", font=("微软雅黑", 16))
appNameLabel.pack(side=LEFT, anchor=CENTER)
appVersionLabel = tk.Label(appInfoTopFrame, text=version, background="white", font=("微软雅黑", 10))
appVersionLabel.pack(side=LEFT, anchor=S)

appMessageLabel = tk.Label(appInfoFrame, text=appinfo["about"], background="white", justify=LEFT)
appMessageLabel.pack(fill=X, side=BOTTOM, anchor=S)

mainFrame = Frame(window)
mainFrame.pack(fill=BOTH, expand=YES, padx=5, pady=5)

"""
authorLabel = Label(mainFrame, text="作者：%s" % appinfo["author"])
authorLabel.pack(padx=5, pady=5, fill=X)
"""

moreText = ScrolledText(window, width=50, height=15, font=("微软雅黑", 9))
moreText.pack(padx=10, pady=10, fill=BOTH)

moreTextList = (
    ("作者", appinfo.get("author", "匿名")), ("版本", version), ("官网", appinfo.get("appUrl")),
    ("开源地址", appinfo.get("gitUrl")))
for item in moreTextList:
    if item[1]:
        moreText.insert(END, "%s：%s\n" % item)

moreText.insert(END, "\n开源许可：\n")

for item in appinfo["license"]:
    moreText.insert(END, "  %s：%s\n" % item)

moreText.insert(END, "\n工具版本：\n")

for item in appinfo["toolsVer"]:
    moreText.insert(END, "  %s：%s\n" % item)

window.centerWindow()
window.iconbitmap("icon.ico")
window.mainloop()
