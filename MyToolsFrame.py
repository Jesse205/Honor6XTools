from tkinter import *
from tkinter.ttk import Frame, LabelFrame, Button, Style


# from tkinter.tix import Button

# Style().configure("TButton", padding=10, relief="flat")


class MyToolsFrame(Frame):
    maxButtonColumn = 10
    buttonWidth = 12

    def __init__(self, master=None, **kw):
        super().__init__(master, **kw)
        self.groups = []
        self.buttons = []

    def setMaxButtonColumn(self, maxColumn):
        self.maxButtonColumn = maxColumn

    def getMaxButtonColumn(self):
        return self.maxButtonColumn

    def setButtonWidth(self, width):
        self.buttonWidth = width

    def getButtonWidth(self, width):
        return self.buttonWidth

    def addGroup(self, title=""):
        labelFrame = LabelFrame(self, text=title)
        labelFrame.pack(padx=5, pady=5, fill=X)
        frame = Frame(labelFrame)
        frame.pack(padx=5, pady=5, fill=X)
        self.groups.append({"Label": labelFrame, "frame": frame})
        # self._addLineFrame(frame)
        self._nowGroup = frame
        self._nowButtonRow, self._nowButtonColumn = 1, 0

    def addButton(self, **kw):
        if self._nowButtonColumn >= self.maxButtonColumn:
            self._nowButtonRow += 1
            self._nowButtonColumn = 0
            # self._addLineFrame(self._nowGroup)

        self._nowButtonColumn += 1
        if "width" in kw:
            width = kw.pop("width")
        else:
            width = self.buttonWidth
        if "ipady" in kw:
            ipady = kw.pop("ipady")
        else:
            ipady = 5
        button = Button(self._nowGroup, width=width, **kw)
        button.grid(row=self._nowButtonRow, column=self._nowButtonColumn, padx=5, pady=5, ipady=ipady)  #

        # button["text"]=text
        # button.pack(side=LEFT, padx=5, expand=NO, ipady=5)
        self.buttons.append(button)
        return button


"""
    def _addLineFrame(self, frame):

        chidFrame = Frame(frame)
        chidFrame.pack(pady=5, fill=X)
        self._nowLine = chidFrame
"""
