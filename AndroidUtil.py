import subprocess


class ADB:
    device = {"adb": "(auto)", "fastboot": "(auto)"}
    English2Chinese = {
        "insufficient permissions for devices": "设备权限不足",
        "daemon started successfully": "后台程序已成功启动",
        "failed to connect to": "连接此设备失败：",
        "device offline": "设备离线",
        "error": "错误",
        "failed to stat file": "未能统计文件",
        "No such file or directory": "没有此类文件或目录",
    }

    @staticmethod
    def stopServer():
        print("ADB StopServer")
        result = subprocess.getstatusoutput("taskkill /F /IM adb.exe")
        print("ADB StopServer", result)
        return result

    @staticmethod
    def contentWebDevice(ip, port):
        print("Connect ip=%s port=%s" % (ip, port))
        result = subprocess.getoutput("adb connect %s:%s" % (ip, port))
        result = ("failed" in result.lower(), result)
        print("Connect", result)
        return result

    @staticmethod
    def getDeviceByMode(mode):
        return ADB.device.get(mode, "(auto)")

    @staticmethod
    def device2cmd(mode):
        device = ADB.getDeviceByMode(mode)
        if device == "(auto)":
            return ""
        else:
            return "-s %s " % device

    @staticmethod
    def isDeviceConnected(mode, devicesList=None):
        device = ADB.getDeviceByMode(mode)
        if devicesList == None:
            devices = ADB.getDevicesList(mode)
        else:
            devices = devicesList
        if device == "(auto)":
            return len(devices) != 0
        else:
            return device in devices

    @staticmethod
    def english2Chinese(value):
        for index, content in ADB.English2Chinese.items():
            value = value.replace(index, content)
        return value

    @staticmethod
    def getDevicesList(mode="all"):
        if mode == "all":
            # subprocess.run("adb\\adb start-server")
            # subprocess.run("adb\\fastboot start-server")
            content = subprocess.getoutput('adb devices')
            content += subprocess.getoutput('fastboot devices')
        else:
            # subprocess.run("adb\\%s start-server"% mode)
            content = subprocess.getoutput("%s devices" % mode)
        print("list",content)
        deviceList = content.strip().split("\n")
        """removeList = (
            "List of devices attached", "* daemon not running; starting now at tcp:5037", "* daemon started successfully")
        for item in removeList:
            if item in deviceList:
                deviceList.remove(item)"""
        result = {}
        for item in deviceList:
            itemList = item.split("\t")
            if len(itemList) == 2:
                result[itemList[0]] = itemList[1]

        print("List of devices attached", result)
        return result


# import re


def installApk(path, config=[]):
    print("Install apk confg=%s %s" % (str(config), path))
    result = subprocess.getstatusoutput(
        'adb %s install %s"%s"' % (ADB.device2cmd("adb"), " ".join(config), path))
    print("Install apk", result)
    return result


def sideload(path):
    print("Sideload", path)
    result = subprocess.getstatusoutput('adb %ssideload "%s"' % (ADB.device2cmd("adb"), path))
    print("Sideload", result)
    return result


def reboot(now, mode=""):
    print("Reboot now=%s,mode=%s" % (now, mode))
    result = subprocess.getstatusoutput("%s %sreboot %s" % (now, ADB.device2cmd(now), mode))
    print("Reboot", result)
    return result


def setOverscan(left="0", top="0", right="0", bottom="0"):
    print("Overscan", "left=%s top=%s right=%s bottom=%s" % (left, top, right, bottom))
    result = subprocess.getoutput(
        "adb\\adb %sshell wm overscan %s,%s,%s,%s" % (ADB.device2cmd("adb"), left, top, right, bottom))
    result = ("error" in result.lower(), result)
    print("Overscan", result)
    return result


def setResolvingPower(size):
    print("Resolving power size %s" % size)
    result = subprocess.getoutput("adb %sshell wm size %s" % (ADB.device2cmd("adb"), size))
    result = ("error" in result.lower(), result)
    print("Resolving power", result)
    return result


def setDpi(dpi):
    print("Dpi %s" % dpi)
    result = subprocess.getoutput("adb %sshell wm density %s" % (ADB.device2cmd("adb"), dpi))
    result = ("error" in result.lower(), result)
    print("Dpi", result)
    return result
