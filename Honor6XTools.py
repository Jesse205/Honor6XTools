from Jesse205 import *

import allTools
import clickEvents
import sub.deviceManager
# import os

window = MyWindow()
window.title(appinfo["appName"])
window.fixDpi()
# window.iconbitmap("icon.ico")

window.resizable(False, False)

clickEvents.window = window


mainFrame = Frame()
mainFrame.pack(padx=5, pady=5)
toolsFrame = MyToolsFrame(mainFrame)
toolsFrame.setMaxButtonColumn(4)
toolsFrame.pack(padx=5, pady=5,side=LEFT,anchor=N)
for group in allTools.tools:
    toolsFrame.addGroup(title=group[0])
    for item in group[1]:
        button = toolsFrame.addButton(text=item[0], command=item[1], state=NORMAL if item[1] else DISABLED)
        if len(item) == 3:
            # balloon.bind_widget(button, balloonmsg=item[2], statusmsg=None)
            ToolTip(button, text=item[2])
sub.deviceManager.loadPage(mainFrame,window)
"""
for i in range(2):
    toolsFrame.addGroup(title="test")
    for i in range(10):
        toolsFrame.addButton(text=str(i + 1))
"""
menuBar = Menu(window)
rebootMenu = Menu(window, tearoff=False)
rebootMenu.add_command(label="系统", command=lambda: AppEvent.reboot(window, now="adb"))
rebootMenu.add_command(label="Recovery", command=lambda: AppEvent.reboot(window, now="adb", mode="recovery"))
rebootMenu.add_command(label="eRecovery", command=lambda: AppEvent.reboot(window, now="adb", mode="erecovery"))
rebootMenu.add_command(label="FastBoot", command=lambda: AppEvent.reboot(window, now="adb", mode="bootloader"))
rebootMenu.add_separator()
rebootMenu.add_command(label="系统", command=lambda: AppEvent.reboot(window, now="fastboot"))
rebootMenu.add_command(label="FastBoot", command=lambda: AppEvent.reboot(window, now="fastboot", mode="bootloader"))
rebootMenu.add_separator()
rebootMenu.add_command(label="更多", command=clickEvents.reboot)
menuBar.add_cascade(label="重启", menu=rebootMenu)

adbMenu = Menu(window, tearoff=False)
adbMenu.add_command(label="终止进程", command=clickEvents.stopServer)
adbMenu.add_command(label="安装环境", command=clickEvents.installAdb)
menuBar.add_cascade(label="ADB", menu=adbMenu)
#menuBar.add_command(label="设备管理", command=clickEvents.deviceManager)
menuBar.add_command(label="关于", command=clickEvents.about)

window.config(menu=menuBar)

window.centerWindow()
window.iconbitmap("icon.ico")

window.mainloop()
