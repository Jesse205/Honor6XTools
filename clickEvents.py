import MyUtils
from Jesse205 import *

window = None


# import threading

def about():
    MyUtils.newWindow("about")
    # runpy.run_module("about", run_name='__main__')


def deviceManager():
    MyUtils.newSubWindow("deviceManager")
    # runpy.run_module("tools.deviceManager", run_name='__main__')


def HuaweiUpdateExtractor():
    MyUtils.startExe(r"tools\HuaweiUpdateExtractor", "HuaweiUpdateExtractor.exe")


def cmd():
    # subprocess.run("start tools\\cmd.bat", shell=True)
    MyUtils.startExe("shellTools", "cmd.bat")


def reboot():
    MyUtils.newSubWindow("reboot")
    # runpy.run_module("tools.reboot", run_name='__main__')


def shell():
    if AppEvent.checkRunAdbCommand(window):
        MyUtils.startExe("shellTools", "shell.bat", values=AndroidUtil.ADB.device2cmd("adb"))


def startShizuku():
    MyUtils.newSubWindow("startShizuku")
    # runpy.run_module("tools.startShizuku", run_name='__main__')


def systemUI():
    MyUtils.newSubWindow("systemUI")
    # runpy.run_module("tools.systemUI", run_name='__main__')


def IDT():
    MyUtils.startExe(r"tools\IDT", "IDT.exe")


def sideload():
    MyUtils.newSubWindow("sideload")
    # runpy.run_module("tools.sideload", run_name='__main__')


def FirmwareFinder():
    MyUtils.startExe(r"tools\FirmwareFinder", "FirmwareFinder.exe")

def PotatoNV():
    MyUtils.startExe(r"tools\PotatoNV", "PotatoNV-next.exe")

def MultiUpgradeSoftwarer():
    MyUtils.startExe(r"tools\HUAWEI  Multi-Upgrade Software", "APKBMUGC01Ver1000.exe")


def scrcpy():
    MyUtils.newSubWindow("scrcpyGUI")
    # runpy.run_module("tools.scrcpyGUI", run_name='__main__')
    """
    if AppEvent.checkRunAdbCommand():
        MyUtils.startExe(r"tools\scrcpy", r"scrcpy-noconsole.exe %s"%AndroidUtil.device2cmd("adb"))
        """


def stopServer():
    AppEvent.generalEvents(window, "终止", {"function": AndroidUtil.ADB.stopServer}, allowNoPhone=True, showSuccess=True,
                           showResult=True)

def installAdb():
    MyUtils.startExe(".\\", "Android_SDK_Platform_Tools_%s.exe"%(appinfo["toolsVer"][1][1]))