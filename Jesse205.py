from tkinter import messagebox

from MyWindow import MyWindow, MyToplevel
from MyToolsFrame import MyToolsFrame
from MyEntry import *
from MyIcon import MyIcon
import MyUtils
from ToolTip import ToolTip

from tkinter import *
from tkinter.ttk import *

from tkinter import filedialog

import AndroidUtil

import os

from appInfo import appinfo
import webbrowser

# print(appinfo["about"])

# 调用浏览器打开链接
def openUrl(url):
    webbrowser.open_new_tab(url)

class AppDialogs:
    @staticmethod
    def noDevicesFound(master=None):
        messagebox.showwarning("提示", "手机未连接", master=master)

    @staticmethod
    def moreThanOneDevice(master=None):
        messagebox.showwarning("提示", "有多个设备/模拟器，请选择指定设备。", master=master)


class AppEvent:
    @staticmethod
    def checkRunAdbCommand(master=None, phoneType="adb", devicesList=None, stateType=None):
        devicesList = devicesList or AndroidUtil.ADB.getDevicesList(phoneType)
        device = AndroidUtil.ADB.getDeviceByMode(phoneType)
        if AndroidUtil.ADB.isDeviceConnected(phoneType, devicesList):
            if (len(devicesList) == 1 and (stateType == None or (stateType in devicesList))) or (
                    device != "(auto)" and (stateType == None or devicesList.get(device) == stateType)):
                return True
            else:
                AppDialogs.moreThanOneDevice(master)
                return False
        else:
            AppDialogs.noDevicesFound(master)
            return False

    @staticmethod
    def generalEvents(master, event, func, showSuccess=False, allowNoPhone=False, showResult=False,
                      phoneType="adb",stateType=None):
        def runFunc():
            result = func["function"](*func.get("args", ()), **func.get("kwargs", {}))
            if result[0]:
                messagebox.showerror(event + "失败", AndroidUtil.ADB.english2Chinese(result[1]), master=master)
                return False
            else:
                if showSuccess:
                    if showResult:
                        messagebox.showinfo(event + "成功", AndroidUtil.ADB.english2Chinese(result[1]), master=master)
                    else:
                        messagebox.showinfo("提示", event + "成功", master=master)
                return True

        if allowNoPhone:
            runFunc()
        else:
            allowRun = AppEvent.checkRunAdbCommand(master,phoneType=phoneType,stateType=stateType)
            if allowRun:
                runFunc()
        """
        if allowNoPhone or AndroidUtil.ADB.isDeviceConnected(phoneType, devicesList):
            if allowNoPhone or len(devicesList) == 1 or (AndroidUtil.ADB.getDeviceByMode(phoneType) != "(auto)"):
                #result = getResult["function"](*getResult.get("args", ()), **getResult.get("kwargs", {}))
                if result[0]:
                    messagebox.showerror(event + "失败", AndroidUtil.ADB.english2Chinese(result[1]), master=master)
                    return False
                else:
                    if showSuccess:
                        if showResult:
                            messagebox.showinfo(event + "成功", AndroidUtil.ADB.english2Chinese(result[1]), master=master)
                        else:
                            messagebox.showinfo("提示", event + "成功", master=master)
                    return True
            else:
                AppDialogs.moreThanOneDevice(master)
                return False
        else:
            AppDialogs.noDevicesFound(master)
            return False"""

    @staticmethod
    def overscanSet(master, **kw):

        return AppEvent.generalEvents(master, "调节", {"function": AndroidUtil.setOverscan, "kwargs": kw})

    @staticmethod
    def reboot(master, now, mode=""):
        print("reboot", now, mode)
        return AppEvent.generalEvents(master, "重启", {"function": AndroidUtil.reboot, "args": (now, mode)},
                                      phoneType=now)

    @staticmethod
    def sideload(master, path):
        return AppEvent.generalEvents(master, "Sideload", {"function": AndroidUtil.sideload, "args": (path,)})

    @staticmethod
    def setResolvingPower(master, size):
        return AppEvent.generalEvents(master, "调节", {"function": AndroidUtil.setResolvingPower, "args": (size,)})

    @staticmethod
    def setDpi(master, dpi):
        return AppEvent.generalEvents(master, "调节", {"function": AndroidUtil.setDpi, "args": (dpi,)})

    @staticmethod
    def contentWebDevice(master, ip, port):
        return AppEvent.generalEvents(master, "连接", {"function": AndroidUtil.ADB.contentWebDevice, "args": (ip, port)},
                                      True, True)