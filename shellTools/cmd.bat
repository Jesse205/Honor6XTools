::chcp 65001
@title 命令提示符
@echo.欢迎使用命令提示符工具！
@echo.
@echo. 输入“adb help”可以查看adb使用帮助
@echo. 输入“fastboot help”可以查看fastboot使用帮助
@echo. 输入“scrcpy --help”可以查看scrcpy使用帮助
@echo. 输入“help”可以查看cmd使用帮助
@echo.
@call cmd.exe
@exit