# from tkinter import Tk, Toplevel
from tkinter import Tk, Toplevel
import ctypes


class BaseWindow:
    @staticmethod
    def getScaleFactor():
        try:
            return ctypes.windll.shcore.GetScaleFactorForDevice(0)
        except:
            return None

    def fixDpi(self):
        scaleFactor = self.getScaleFactor()
        if scaleFactor:
            try:
                ctypes.windll.shcore.SetProcessDpiAwareness(2)
                self.tk.call('tk', 'scaling', scaleFactor / 75)
            except:
                pass

    def size(self, width, height):
        self.geometry("%dx%d" % (width, height))

    def centerWindow(self, width=None, height=None):
        # self.update()
        if (not width) and (not height):
            self.update_idletasks()

        width, height = width or self.winfo_reqwidth(), height or self.winfo_reqheight()
        screenWidth, screenHeight = self.maxsize()
        x = round((screenWidth - width) / 2)
        y = round((screenHeight - height) / 2)
        self.geometry("+%s+%s" % (x, y))
        self.update_idletasks()


class MyWindow(BaseWindow, Tk):
    pass


class MyToplevel(BaseWindow, Toplevel):
    pass
