import subprocess
import runpy


def startExe(path, name, values="", encoding="gbk"):
    print("cmd","start \"\" \"%s\" %s &exit" % (name, values))
    subprocess.run("start \"\" \"%s\" %s &exit" % (name, values), cwd=path, shell=True, encoding=encoding)
    # return mysubprocess.getstatusoutput("start %s /i %s &exit" % (name,values), cwd=path, encoding=encoding)


def newWindow(name):
    runpy.run_module(name, run_name='__main__')


def newSubWindow(name):
    newWindow("sub." + name)
